import file_manager
from django.http import HttpResponse
from fbrowser100M.forms import UploadFileForm
from pbrowser.views import filter_projects
from pbrowser.models import Files100M, Project
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, HttpResponseRedirect, reverse

from pbrowser.views import upload_file
from .serializers import ProjectSerializer
from rest_framework import viewsets


@login_required(login_url='/admin/login/')
def index(request):
    if request.method == 'POST':
        return upload_file(request)
    projects = filter_projects(request.user)
    folders = list()
    files = list()
    if len(projects) > 0:
        for project in projects:
            folders, files = file_manager.scan_dir(request.user, project.name)
    else:
        projects = list()
        folders = list()

    return render(request, 'index.html', {"projects": projects,
                                          "folders": folders,
                                          "files": files,
                                          'file_upload_form': None})


class ProjectView(viewsets.ModelViewSet):
    serializer_class = ProjectSerializer
    queryset = Project.objects.all()
