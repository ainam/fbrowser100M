# THIS FILE IS GENERATED FROM DK SETUP.PY
#
import os
short_version = '0.0.3'
version = '0.0.3'
full_version = '0.0.3'
git_revision = os.popen('git rev-parse HEAD').read()
release = False

if not release:
    version = full_version
