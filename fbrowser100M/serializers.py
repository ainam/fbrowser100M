from rest_framework import serializers
from pbrowser.models import Project


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ('id', 'date', 'kuerzel', 'number',
                  'number', 'name', 'user', 'category')
