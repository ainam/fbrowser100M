# The context processor function
def get_version(request):
    from fbrowser100M.version import version
    return {
        'get_version': version,
    }


def git_revision(request):
    from fbrowser100M.version import git_revision
    return {
        'git_revision': git_revision,
    }
