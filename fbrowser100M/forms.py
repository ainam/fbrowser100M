import os
from django import forms
from file_manager import check_name
from pbrowser.models import Category, Files100M, Project


class UploadFileForm(forms.ModelForm):
    ALLOWED_TYPES = ['pdf', 'dwg']

    class Meta:
        model = Files100M
        fields = ['project', 'category', 'file']

    def verify_name(self):
        file = self.cleaned_data.get('file', None)
        extension = os.path.splitext(file.name)[1][1:].lower()
        if extension in self.ALLOWED_TYPES:
            return check_name(file,
                              self.cleaned_data['project'],
                              self.cleaned_data['category'])
        else:
            return "Only PDF or DWG supported!"


class ProjectCopyForm(forms.Form):
    project = forms.ModelChoiceField(queryset=Project.objects.all())
    kuerzel = forms.CharField(max_length=3)
    number =  forms.CharField(max_length=3)
    name =  forms.CharField(max_length=30)    