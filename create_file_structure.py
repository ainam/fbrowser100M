import os
import sys
from re import sub


ROOT_DIR = "uploads"

TOP_DIR = "Bauvorhaben"
FOLDER_STRUCTURE = {"Lageplaene": {"sublevel": None, "abb": "LGP"},
                    "Bauvoranfrage": {"sublevel": None, "abb": "BAUV"},
                    "Bauantrag": {"sublevel": None, "abb": "BAU"},
                    "Baugenehmigung": {"sublevel": None, "abb": "BAUG"},
                    "Gutachter_Sachverstaendige": {"sublevel": {"Brandschutz": {"sublevel": None, "abb": "BSV"},
                                                                "Lüftung": {"sublevel": None, "abb": "LSV"},
                                                                "BMA": {"sublevel": None, "abb": "BMASV"},
                                                                "Prüfstatiker": {"sublevel": None, "abb": "PSTAT"},
                                                                "SiGeKo": {"sublevel": None, "abb": "SGK"},
                                                                "Bodengutachten": {"sublevel": None, "abb": "BDG"},
                                                                "Vermessung": {"sublevel": None, "abb": "VER"}},
                                                   "abb": "AAAAAA"},
                    "Planer":  {"sublevel": {"Entwässerung": {"sublevel": {"Berechnung": {"sublevel": None, "abb": "ENTW-B"},
                                                                           "Pläne": {"sublevel": None, "abb": "ENTW-P"}},
                                                              "abb": "AAAAAA"},
                                             "Architektur LPH 2-4": {"sublevel": None, "abb": "ARC-E"},
                                             "Architektur LPH 5": {"sublevel": None, "abb": "ARC-A"},
                                             "Brandschutznachweis": {"sublevel": {"Konzept": {"sublevel": None, "abb": "BRA-Brandschutznachweis"},
                                                                                  "Pläne": {"sublevel": None, "abb": "BRA-GR"}},
                                                                     "abb": "AAAAAA"},
                                             "Schallschutznachweis": {"sublevel": {"Berechnung": {"sublevel": None, "abb": "SCH-Schallschutznachweis"},
                                                                                   "Pläne": {"sublevel": None, "abb": "SCH-P"}},
                                                                      "abb": "AAAAAA"},
                                             "Wärmeschutznachweis": {"sublevel": {"Berechnung": {"sublevel": None, "abb": "WAE-B"},
                                                                                  "Pläne": {"sublevel": None, "abb": "WAE-P"}},
                                                                     "abb": "AAAAAA"},
                                             "TGA": {"sublevel": {"Elektro": {"sublevel": None, "abb": "FP-E"},
                                                                  "Heizung": {"sublevel": None, "abb": "FP-H"},
                                                                  "Lüftung": {"sublevel": None, "abb": "FP-L"},
                                                                  "Sanitär": {"sublevel": None, "abb": "FP-S"},
                                                                  "Klima": {"sublevel": None, "abb": "FP-K"}},
                                                     "abb": "AAAAAA"},

                                             "Statik": {"sublevel": {"Berechnung": {"sublevel": None, "abb": "FP-TWP-B"},
                                                                     "Pläne": {"sublevel": None, "abb": "FP-TWP-P"}, },
                                                        "abb": "AAAAAA"},

                                             "Aufzug": {"sublevel": {"Berechnung": {"sublevel": None, "abb": "FP-AFZ-B"},
                                                                     "Pläne": {"sublevel": None, "abb": "FP-AFZ-P"}},
                                                        "abb": "AAAAAA"}},
                                "abb": "AAAAAA"}
                    }


OS_SEPERATOR = '/'
if sys.platform == 'win32':
    OS_SEPERATOR = '\\'


def mkdir(top_dir, dir_name=""):
    top_dir.replace("//", "/")
    if dir_name not in os.listdir(top_dir):
        os.mkdir(os.path.join(top_dir, dir_name))
        level = os.path.join(top_dir, dir_name).count(OS_SEPERATOR) - 1
        return [level, dir_name, os.path.join(top_dir, dir_name)]


def create_structure(project_name):
    cats = list()
    cats.append(mkdir(ROOT_DIR, project_name))
    for level1 in FOLDER_STRUCTURE.keys():
        cats.append(mkdir(os.path.join(ROOT_DIR, project_name), level1))
        if cats[-1]:
            cats[-1].append(FOLDER_STRUCTURE[level1]["abb"])
        print(cats[-1])
        sublevel = FOLDER_STRUCTURE[level1]
        if sublevel["sublevel"]:
            sublevel = list(sublevel["sublevel"].keys())
            for level2 in sublevel:
                cats.append(
                    mkdir(os.path.join(ROOT_DIR, project_name, level1), level2))
                if cats[-1]:
                    cats[-1].append(FOLDER_STRUCTURE[level1]
                                    ["sublevel"][level2]["abb"])
                if FOLDER_STRUCTURE[level1]["sublevel"][level2]["sublevel"]:
                    for level3 in FOLDER_STRUCTURE[level1]["sublevel"][level2]["sublevel"]:
                        cats.append(mkdir(os.path.join(ROOT_DIR, project_name,
                                                       level1, level2), level3))
                        if cats[-1]:
                            cats[-1].append(FOLDER_STRUCTURE[level1]["sublevel"]
                                            [level2]["sublevel"][level3]["abb"])
    return cats


def create_test_files():
    from fpdf import FPDF
    from django.core.files import File
    from pbrowser.models import Files100M, Project

    def create_pdf(prj, cat_abb):
        if cat_abb != "AAAAAA":
            cat = prj.cat_struct.category.get(kuerzel=cat_abb)
            name = "{}-{}-{}.pdf".format(
                prj.number, prj.kuerzel, cat_abb)
            print("create ", name)

            pdf = FPDF()
            pdf.add_page()
            pdf.set_xy(0, 0)
            pdf.set_font('arial', 'B', 13.0)
            pdf.cell(ln=0, h=5.0, align='L', w=0, txt=name, border=0)
            pdf.output(name, 'F')
            f = File(open(name, 'rb'))
            fid = Files100M(file=f, project=prj, category=cat)
            fid.save()

    # p = Project(number="070", kuerzel="FT", name="Frankenthal")
    # p.save()
    # p = Project(number="072", kuerzel="FL", name="Florstadt")
    # p.save()
    # p = Project(number="073", kuerzel="NH", name="Sadiq Moschee Nordhorn")
    # p.save()
    # p = Project(number="074", kuerzel="RN", name="Raunheim")
    # p.save()
    for prj in Project.objects.all():

        for folder in FOLDER_STRUCTURE.keys():
            mkdir("Tests/", prj.name)
            level1 = FOLDER_STRUCTURE[folder]
            create_pdf(prj, level1["abb"])

            if level1["sublevel"]:
                for level2 in level1["sublevel"].keys():
                    level2 = level1["sublevel"][level2]
                    create_pdf(prj, level2["abb"])
                    if level2["sublevel"]:
                        for level3 in level2["sublevel"].keys():
                            level3 = level2["sublevel"][level3]
                            create_pdf(prj, level3["abb"])


if __name__ == "__main__":

    # create_structure("Rostock")
    create_test_files()
