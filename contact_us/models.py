from django.db import models
from django.utils import timezone
from django.core.mail import send_mail
from django.core.validators import *
from django.contrib.auth import get_user_model

from fbrowser100M import settings


class ContactUs(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200)
    subject = models.CharField(max_length=200)
    TYPE_OF_CONTACT = (('is', 'Issue'),
                       ('cr', 'Change request'),
                       ('fe', 'Feedback'),
                       ('in', 'Inquiry'),
                       ('ot', 'Ohter'))
    type = models.CharField(max_length=2, choices=TYPE_OF_CONTACT,
                            default='fe')
    date_opened = models.DateTimeField(
        'Date', default=timezone.now, editable=False)
    message = models.TextField(default='', max_length=1000)
    TYPE_OF_PROGRESS = (('op', 'open'),
                        ('ip', 'in progress'),
                        ('cl', 'closed'),
                        ('re', 'Review'))
    progress = models.CharField(max_length=2, choices=TYPE_OF_PROGRESS,
                                default='open')

    def get_all_values(self):
        return [str(self.number), self.name, self.Title, self.type, str(self.date_opened), self.message, self.progress]

    def __str__(self):
        return ', '.join(self.get_all_values())

    def save(self, *args, **kwargs):
        # this will take care of the saving
        super(ContactUs, self).save(*args, **kwargs)
        # do email stuff
        print('Emailing!')
        send_mail(
            self.subject,
            self.message,
            settings.EMAIL_HOST_USER,
            ['annasinam@zoho.com'],
            fail_silently=True,
        )
