from django import forms
from django.forms import ModelForm
from django.contrib.admin import widgets
from .models import ContactUs


class ContactUsForm(forms.ModelForm):
    class Meta:
        model = ContactUs
        fields = ['name', 'subject', 'type', 'message']