from django.contrib import admin
from .models import ContactUs


class ContactUsAdmin(admin.ModelAdmin):
    # ...
    list_display = ('id', 'name', 'subject', 'type', 'date_opened', 'message', 'progress')


admin.site.register(ContactUs, ContactUsAdmin)