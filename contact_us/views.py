from django.shortcuts import render
from django.http import HttpResponse
from contact_us.forms import ContactUsForm


def about(request):
    if request.method == 'POST':
        form = ContactUsForm(request.POST)
        if form.is_valid():
            form.save()
    form = ContactUsForm()
    return render(request, 'about.html', {'form': form})


def contact_us(request):
    return render(request, '/admin/contact_us/')
