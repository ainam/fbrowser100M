import os
from requests import request
from create_file_structure import FOLDER_STRUCTURE, TOP_DIR, OS_SEPERATOR


FILE_NAME_CHECK_ERR_MSGS = ["Project Number wrong based on entered Project",
                            "category Tag incorrect based on category selected",
                            "File Index is not correct, the index of "
                            "uploaded file already exists!"
                            ]
FILE_NAME_CHECK_ERR_MSGS = {"prj_no": "Projekt Nummer stimmt mit dem "
                            "gewählten Projekt nicht überein",
                            "prj_ab": "Projekt Kürzel stimmt mit dem "
                            "gewählten Projekt nicht überein",
                            "category": "Kategorie Kürzel stimmt mit der "
                            "gewählten Kategorie nicht überein",
                            "idx": "Datei Index ist inkorrekt, Datei mit "
                            "diesem Index existiert bereits"
                            }


def check_name(file, project, category):
    name = file.name
    if not name.startswith(project.number):
        return FILE_NAME_CHECK_ERR_MSGS['prj_no']
    name = name[name.find('-') + 1:]
    if not name.startswith(project.kuerzel):
        return FILE_NAME_CHECK_ERR_MSGS['prj_ab']
    name = name[name.find('-') + 1:]
    if not name.startswith(category.kuerzel):
        return FILE_NAME_CHECK_ERR_MSGS['category']
    path = os.path.join("uploads", project.name, TOP_DIR,
                        category.name, file.name)
    if os.path.exists(path):
        return FILE_NAME_CHECK_ERR_MSGS['idx']
    return None


def get_file_description(instance):

    def get_level(name):
        if name == "U2":
            return " Parkunterhaus"
        elif name == "U1":
            return " Keller"            
        elif name == "00":
            return " Erdgeschoss"            
        elif name == "01":
            return " 1. Obergeschoss"
        elif name == "02":
            return " 2. Obergeschoss"
        else:
            return name

    def get_direction(name):
        if name == "NO":
            return " Nord-Ost"
        elif name == "SO":
            return " Süd-Ost"
        elif name == "NW":
            return " Nord-West"
        elif name == "SW":
            return " Süd-West"
    
    description = ""
    name = instance.filename().split('-')[2:]      # prj number and abbreviation disregarded

    index = 1   
    if name[0] == "LGP":
        description += " Lageplan"
    elif name[0] == "BAUV":
        description += " Bauvoranfrage"
    elif name[0] == "BAUA":
        description += " Bauantrag"
    elif name[0] == "BAUG":
        description += " Baugenehmigung"
    # Folder Structure Gutachter Sachvestaendige
    elif name[0] == "BSV":        
        description += " Brandschutz Sachverständiger"
    elif name[0] == "LSV":
        description += " Lüftung Sachverständiger"
    elif name[0] == "BMASV":
        description += " BMA Sachverständiger"
    elif name[0] == "PSTAT":
        description += " Prüfstatik"
    elif name[0] == "SGK":
        description += " SiGeKo"
    elif name[0] == "BDG":
        description += " Bodengutachten"
    elif name[0] == "VER":
        description += " Vermessung"
    # Folder Planer
    elif name[0] == "ENTW":
        description += " Entwässerung"
        index += 1
    elif name[0] == "ARC":
        description = "Architektur"
        if name[index] == "E":
            description += " Entwurf"
        if name[index] == "A":
            description += " Ausführung"
        index += 1
    elif name[0] == "BRA":
        description += " Brandschutz"
    elif name[0] == "SHA":
        description += " Schallschutz"
    elif name[0] == "WAE":
        description += " Wärmeschutz"
    elif name[0] == "FP":
        description += " Fachplaner"
        index += 1
        if name[index] == "E":
            description += " Elektro"
        elif name[index] == "H":
            description += " Heizung"
        elif name[index] == "L":
            description += " Lüftung"
        elif name[index] == "S":
            description += " Sanitär"
        elif name[index] == "K":
            description += " Klima"
        elif name[index] == "TWP":
            description += " Statik"
        elif name[index] == "AFZ":
            description += " Aufzug"
        elif name[index] == "SH":
            description += " Schema"

    if name[index] == "DT":
        description += " Detail"
    elif name[index] == "GR":
        description += " Grundriss"
        description += get_level(name[index+1])
    elif name[index] == "DS":
        description += " Deckenspiegel"
    elif name[index] == "AN":
        description += " Ansicht"
        description += get_direction(name[index+1])
    return description


def handle_uploaded_file(instance, filename):
    ext = filename.split('.')[-1]
    print(filename)
    fullname = os.path.join('uploads/', instance.project.name,
                            TOP_DIR, instance.category.name,
                            filename)
    print("get fullname: {}".format(fullname))
    # fullname += instance.version
    return fullname


def get_files(user, project, folder, subfolder):
    files = list()
    dir = project.name + "/" + folder + "/" + subfolder
    with os.scandir("uploads/{}".format(dir)) as it:
        for entry in it:
            if not entry.name.startswith('.') and entry.is_file():
                files.append(entry)
    return files


def scan_dir(user, dir=""):
    files = list()
    folders = list()
    with os.scandir("uploads/{}".format(dir)) as it:
        for entry in it:
            if not entry.name.startswith('.') and entry.is_file():
                files.append(entry)
            elif entry.is_dir():
                folders.append(entry)

    folders.sort(key=lambda x: x.name)
    return folders, files


def scan_project(user, project):
    subfolders = []
    folders, files = scan_dir(user, project.name)
    for subfolder in folders:
        subfolders, files = scan_dir(
            user, "{}/{}".format(project.name, subfolder.name))
    return {'folders': folders, 'subfolders': subfolders, 'files': files}
