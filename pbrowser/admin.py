from django.contrib import admin
from pbrowser.models import Project, Files100M, Category


# Register your models here.
class ProjectAdmin(admin.ModelAdmin):
    list_display = ('id', 'date', 'kuerzel', 'number', 'name', 'users_str' )
    list_filter = ('kuerzel', 'number', 'name')


admin.site.register(Project, ProjectAdmin)


class Files100MAdmin(admin.ModelAdmin):
    list_display = ('id', 'date', 'project', 'category',
                    'upload_by', 'file', 'description', 'users_str')
    list_filter = ('category', 'project', 'upload_by', 'date')


admin.site.register(Files100M, Files100MAdmin)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'level', 'name', 'kuerzel', 'users')
    list_filter = ('kuerzel', 'level', 'name')


admin.site.register(Category, CategoryAdmin)
