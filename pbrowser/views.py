import imp
from re import template
import file_manager
import django_filters
from django.contrib import messages
from django.shortcuts import render, redirect, reverse
from pbrowser.models import Category, Files100M, Project
from django.http import HttpResponseRedirect, FileResponse
from fbrowser100M.forms import ProjectCopyForm, UploadFileForm


def filter_projects(user):
    projects = list()
    for p in Project.objects.all():
        if user in p.user.all():
            projects.append(p)
    return projects


def browser(request):
    return render(request, '/admin/browser/')


def redirect_view(request):
    response = redirect('/redirect-success/')
    return response


def upload_file(request):
    if request.method == 'POST':
        msg = "help me"
        form = UploadFileForm(request.POST, request.FILES)
        print("Upload File")
        if form.is_valid():
            print("Form is valid")
            msg = form.verify_name()
            if msg:
                print(form.cleaned_data)
                pk = form.cleaned_data['project'].id
                # folder = form.cleaned_data['slug1']
                subfolder = form.cleaned_data['category']
                messages.add_message(request, messages.ERROR,
                                     msg)
                reversed_url = reverse('pbrowser:projects',
                                       kwargs={'pk': pk})

                return HttpResponseRedirect(reversed_url)
            file100M = form.save(commit=False)
            file100M.upload_by = request.user
            file100M.file = request.FILES['file']
            file100M.project = Project.objects.get(
                pk=form.cleaned_data['project'].id)
            # print(file100M.category)
            # file100M.category = file100M.project.cat_struct.category.get(
            #   name=form.cleaned_data['category'].name)
            # print(file100M.category)
            file100M.description = file_manager.get_file_description(file100M)
            file100M.save()
            messages.add_message(request, messages.SUCCESS,
                                 "Upload Successful")
    return HttpResponseRedirect(reverse('index'))


def projects(request, pk=1):
    if request.method == 'POST':
        return upload_file(request)
    project = None
    if Project.objects.filter(pk=pk, user=request.user).exists():
        project = Project.objects.get(pk=pk, user=request.user)
    categories = list()
    for cat in project.cat_struct.category.all():
        if request.user in cat.user.all():
            categories.append(cat)
    file_fields = Files100M._meta.get_fields()

    template_dict = {"projects": filter_projects(request.user),
                     "project": project,
                     "categories": categories,
                     "file_fields": file_fields,
                     'file_upload_form': None}
    return render(request, 'index.html', template_dict)


def category(request, pk_cat=1, pk_proj=1):
    if request.method == 'POST':
        return upload_file(request)
    category = None
    if Category.objects.filter(pk=pk_cat, user=request.user).exists():
        category = Category.objects.get(pk=pk_cat, user=request.user)
    project = Project.objects.get(pk=pk_proj)
    categories = list()
    for cat in project.cat_struct.category.all():
        if request.user in cat.user.all():
            categories.append(cat)
    files = Files100M.objects.filter(
        project=project, category=category)                             # user=request.user to be added if user filter needs to be used
    file_fields = Files100M._meta.get_fields()
    file_upload_form = None
    if request.user.has_perm('pbrowser.can_add_file100M'):
        file_upload_form = UploadFileForm(initial={
            "project": project,
            "category": category,
        })
    template_dict = {"projects": filter_projects(request.user),
                     "project": project,
                     "categories": categories,
                     "file_fields": file_fields,
                     "files": files,
                     'file_upload_form': file_upload_form}
    return render(request, 'index.html', template_dict)


def files(request, pk=1, slug1="", slug2=""):
    if request.method == 'POST':
        return upload_file(request)
    project = Project.objects.get(pk=pk)
    scan_dir = file_manager.scan_project(request.user, project)
    scan_dir['files'] = Files100M.objects.filter(
        project=project, category=slug2)
    scan_dir['file_fields'] = Files100M._meta.get_fields()
    file_upload_form = None
    if request.user.has_perm('pbrowser.can_add_file100M'):
        file_upload_form = UploadFileForm(initial={
            "project": project,
            "category": category,
        })
    template_dict = {"projects": filter_projects(request.user),
                     "project": project,
                     "folders": scan_dir['folders'],
                     "subfolders": scan_dir['subfolders'],
                     "file_fields": scan_dir['file_fields'],
                     "files": scan_dir['files'],
                     'file_upload_form': file_upload_form}
    return render(request, 'index.html', template_dict)


def copy_project(request, pk=1):
    if request.method == 'POST':
        form = ProjectCopyForm(request.POST)
        if form.is_valid():
            prj = Project.objects.get(
                pk=form.cleaned_data['project'].id)
            user_dict = dict()
            for cat in prj.cat_struct.category.all():
                user_dict[cat.kuerzel] = cat.user.all()
            print(user_dict)
            kuerzel = form.cleaned_data['kuerzel']
            number = form.cleaned_data['number']
            name = form.cleaned_data['name']
            project = Project(kuerzel=kuerzel, number=number, name=name)
            project.save(user_dict=user_dict)

    return render(request, 'copy_project.html', {'form': ProjectCopyForm()})


def open_file(request, pk=1):
    file = Files100M.objects.get(id=pk)
    return FileResponse(open(str(file.file), 'rb'), content_type='application/pdf')
