#from ensurepip import version
from django.core.files.storage import FileSystemStorage
from django.conf import settings
import os


class RenameStorage(FileSystemStorage):

    def get_available_name(self, name, max_length=None):
        # If the filename already exists, remove it as if it was a true file system
        if self.exists(name):
            os.rename(name, name + ".r{}{}{}".format(version))
        return name
