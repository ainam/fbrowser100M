import os
import file_manager
from requests import request
import create_file_structure
from django.db import models
from datetime import datetime
from unicodedata import category
from pbrowser.storage import RenameStorage
from file_manager import handle_uploaded_file
from django.contrib.auth.models import User, Group
from django.core.exceptions import ValidationError


def validate_file(name):
    name = str(name)
    name = "###"
    name += "-AB"
    if instance.type.startswith("021"):     # Lageplaene
        name += "-LGB-"
    elif instance.type.startswith("022"):   # Bauvoranfrage
        name += "-BAUV-"
    elif instance.type.startswith("023"):   # Bauantrag
        name += "-BAUA-"
    elif instance.type.startswith("024"):   # Geologe
        name += "-BAUG-"
    elif instance.type.startswith("025"):   # Vermessung
        name += "-VER-Freitext"
    elif instance.type.startswith("026"):   # Entwaesserung
        name += "-ENTW-Freitext"
        name += "-ENTW-GR"
    elif instance.type.startswith("027"):   # Statik
        name += "-FP-TW-Berechnung-"
        name += "-FP-TW-BW-"
    elif instance.type.startswith("028"):   # Elektro
        name += "-BSV-"
    elif instance.type.startswith("029"):   # HLS
        name += "-BSV-"
    elif instance.type.startswith("030"):   # Waermeschutz
        name += "-BSV-"
    elif instance.type.startswith("031"):   # Schallschutz
        name += "-BSV-"
    elif instance.type.startswith("032"):   # Brandschutz
        name += "-BSV-"
    elif instance.type.startswith("033"):   # Architektur-LP2-4
        name += "-BSV-"
    elif instance.type.startswith("034"):   # Architektur-LP5
        name += "-BSV-"
    if name.endswith(".pdf") is False or name.endswith(".dwg") is False:
        raise ValidationError("Only PDF and DWG can be uploaded")
    else:
        return name


def validate_letters(value):
    if not value.isupper():
        raise ValidationError("Nur Großbuchstaben erlaubt")


class Category(models.Model):
    id = models.AutoField(primary_key=True)
    level = models.IntegerField()
    kuerzel = models.CharField(max_length=6, default="AAAAAA",
                               validators=[validate_letters])
    name = models.CharField(max_length=30)
    path = models.CharField(max_length=100, default="uploads/")
    user = models.ManyToManyField(User)

    def __str__(self):
        # return ": ".join([str(self.id), self.name])
        return self.name

    def users(self):
        return ";".join([user.username for user in self.user.all()])


class Project(models.Model):
    id = models.AutoField(primary_key=True)
    date = models.DateTimeField(default=datetime.now, blank=True)
    kuerzel = models.CharField(max_length=3, default="AA",
                               validators=[validate_letters])
    number = models.CharField(max_length=3, default="000")
    name = models.CharField(max_length=30)
    user = models.ManyToManyField(User,
                                  related_name="Projekt_Gruppe")
    category = models.ManyToManyField(Category,
                                      null=True, blank=True, editable=False)

    def save(self, user_dict=None):
        cats = create_file_structure.create_structure(self.name)
        if cats[0]:
            for cat_values in cats:
                kuerzel = self.kuerzel
                if len(cat_values) > 3:
                    kuerzel = cat_values[3]
                cat = Category(
                    level=cat_values[0], name=cat_values[1], path=cat_values[2],
                    kuerzel=kuerzel)
                cat.save()
                if user_dict:
                    cat.user.add(user_dict[cat.kuerzel])
                self.category.add(cat)
        super().save()

    def delete(self):
        print("deleting Project")
        for cat in self.cat_struct.category.all():
            if Files100M.objects.filter(category=cat).exists():
                for file in Files100M.objects.get(category=cat):
                    file.delete()
            cat.delete()
        self.cat_struct.delete()
        os.remove(os.path.join("uploads", self.name))
        super().delete()

    def __str__(self):
        return self.name

    def users_str(self):
        return ";".join([user.username for user in self.user.all()])


# Create your models here.
class Files100M(models.Model):
    id = models.AutoField(primary_key=True)
    description = models.CharField(max_length=300, null=True, blank=True)
    file = models.FileField("File", upload_to=handle_uploaded_file,
                            # storage=RenameStorage(),
                            # validators=[validate_file]
                            )
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    date = models.DateTimeField(default=datetime.now, blank=True)
    upload_by = models.ForeignKey(User, null=True,
                                  related_name="Uploaded_by",
                                  on_delete=models.CASCADE)
    user = models.ManyToManyField(User, related_name="Group")

    def users_str(self):
        return ";".join([user.username for user in self.user.all()])

    def filename(self):
        name = self.file.name.split(create_file_structure.OS_SEPERATOR)
        name, _ = os.path.splitext(name[-1])
        return name

    def dateityp(self):
        _, extension = os.path.splitext(self.file.name)
        return extension
