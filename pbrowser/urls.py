from .views import redirect_view
from django.urls import path, re_path
from django.urls import path, include
from . import views


app_name = "pbrowser"

urlpatterns = [
    path('projects/<int:pk>', views.projects, name='projects'),
    path('upload_file', views.upload_file, name='upload_file'),
    path('open_file/<int:pk>', views.open_file, name='open_file'),
    path('copy_project', views.copy_project, name='copy_project'),
    path('files/<int:pk>/<slug:slug1>/<slug:slug2>', views.files, name='files'),
    path('category/<int:pk_cat>/<int:pk_proj>', views.category, name='category'),
]
