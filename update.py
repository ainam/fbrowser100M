import os
from pickle import FALSE


def update(master=FALSE):
    def increment_number(line):    
        ver = line[line.find("=")+2:]
        ver = ver.strip('\n').strip("'").split('.')
        ver[-1] = str(int(ver[-1]) + 1)
        if master:
            pass
            
        return ".".join(ver)
    lines = list()
    with open("fbrowser100M/version.py", 'r') as f:
        for line in f.readlines():
            if line.startswith("short_version"):
                line = "short_version = '{}'\n".format(increment_number(line))

            if line.startswith("full_version"):
                line = "full_version = '{}'\n".format(increment_number(line))
            if line.startswith("version"):
                line = "version = '{}'\n".format(increment_number(line))
            lines.append(line)
    with open("fbrowser100M/version.py", 'w') as f:

        for line in lines:

            f.write(line)

    os.popen('sudo systemctl daemon-reload;sudo systemctl restart gunicorn')


if __name__ == "__main__":

    update(FALSE)
